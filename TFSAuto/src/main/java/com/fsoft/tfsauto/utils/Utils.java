package com.fsoft.tfsauto.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;

public class Utils {
	
	/**
	 * get table object from Excel
	 * @author ManhND5
	 * @param xlFilePath
	 * @param sheetName
	 * @return
	 */
	public static String[][] getTableObjectFromExcel(String xlFilePath, String sheetName) {
		String[][] tabArray = null;
		int ci, cj;
		try {
			
			WorkbookSettings setting = new WorkbookSettings();
			setting.setEncoding("CP1252");
			Workbook workbook = Workbook.getWorkbook(new File(xlFilePath), setting);
			Sheet sheet = workbook.getSheet(sheetName);
			tabArray = new String[sheet.getRows() - 1][3];
			ci = 0;

			for (int i = 1; i < sheet.getRows(); i++, ci++) {
				cj = 0;
				for (int j = 0; j < sheet.getColumns(); j++, cj++) {
					tabArray[ci][cj] = sheet.getCell(j, i).getContents();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (tabArray);
	}
	
	/**
	 * get List Hash Map Data from Excel
	 * @author ManhND5
	 * @param xlFilePath
	 * @param sheetName
	 * @return
	 */
	public static List<HashMap<String, String>> getListHashMapFromExcel(String xlFilePath, String sheetName) {
		List<HashMap<String, String>> listData = new ArrayList<HashMap<String,String>>();
		try {
			
			WorkbookSettings setting = new WorkbookSettings();
			setting.setEncoding("CP1252");
			Workbook workbook = Workbook.getWorkbook(new File(xlFilePath), setting);
			Sheet sheet = workbook.getSheet(sheetName);
			
			for (int i = 1; i < sheet.getRows(); i++) {
				HashMap<String, String> hmRowData = new HashMap<String, String>();  
				for (int j = 0; j < sheet.getColumns(); j++) {
					hmRowData.put(sheet.getCell(j, 0).getContents(), sheet.getCell(j, i).getContents());
				}
				
				listData.add(hmRowData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return listData;
	}
	
	/**
	 * 
	 * @param datas
	 * @return
	 */
	public static Object[][] dpPrepareData(List<HashMap<String, String>> datas) {
		int size = 0;
    	Object[][] array = new Object[datas.size()][];
    	size = datas.size();
		for(int i=0; i < size; i++) {
			array[i] = new Object[]{datas.get(i)};
		}
		return array;
	}
	
	public static void main(String[] args) {
		getListHashMapFromExcel("src\\main\\resources\\testdata\\testdata.xls", Constants.SHEET_DATA);
	}
}
