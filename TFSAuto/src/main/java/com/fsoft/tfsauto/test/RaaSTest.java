package com.fsoft.tfsauto.test;

import org.testng.annotations.Test;

import com.fsoft.tfsauto.utils.Constants;
import com.fsoft.tfsauto.utils.Utils;

import java.util.HashMap;
import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

public class RaaSTest {
	
	@BeforeClass
	public void beforeClass() {
	}
	
	@DataProvider(name="dpListAccount")
	public Object[][] dpListAccount() {
		return Utils.dpPrepareData(Utils.getListHashMapFromExcel(Constants.PATH_FILE_TEST_DATA
				, Constants.SHEET_DATA));
	}
	@Test(dataProvider = "dpListAccount")
	public void testRegisterAccount(HashMap<String, String> recordData) {
		System.out.println("Name = " + recordData.get("Name"));
		System.out.println("Expresstion = " + recordData.get("Expression"));
		System.out.println("Type = " + recordData.get("Type"));
	}
	
}
